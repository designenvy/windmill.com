/** import external dependencies */
import 'jquery';

import AOS from 'aos';




AOS.init()

$(window).on('load', function () {
    AOS.refresh();
});


$(".menu-item-has-children").click(function() {
  $(this).toggleClass("menu-expanded");
});



$('.introvideo').parent().click(function () {
    if($(this).children(".introvideo").get(0).paused){
        $(this).children(".introvideo").get(0).play();
        $(this).children(".playpause").fadeOut();
    }else{
       $(this).children(".introvideo").get(0).pause();
        $(this).children(".playpause").fadeIn();
    }
});





$(document).ready(function(){
  var d = new Date();
  var n = d.getHours();
  if (n > 22 || n < 4)
    // If time is after 8PM or before 6AM, apply night theme to ‘body’
    $( "body" ).addClass( "night" );
   
  else if (n > 4 && n < 12)
    // If time is between 11aM – 5pM day theme to ‘body’
   $( "body" ).addClass( "morning" );

  else if (n > 11 && n < 17)
    // If time is between 6am – 10aM morning theme to ‘body’
   $( "body" ).addClass( "day" );

  else
    // Else use ‘sunset’ theme
    $( "body" ).addClass( "sunset" );

});

  

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';


/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  /** All pages */
  common,
  /** Home page */
  home,
  /** About Us page, note the change from about-us to aboutUs. */
  aboutUs,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
