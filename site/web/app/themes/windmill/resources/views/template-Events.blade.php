{{--
  Template Name: Events
--}}

@extends('layouts.app')
@section('content')

<header class="page__hero">
<!--==========================
=            logo            =
===========================-->



<div class="hero__tagline"><span class="tagline__caption">Host Your Next</span><h2>Event</h2><span class="tagline__subtitle">At The Legendary <br>American Windmill Museum</span>
</div>





</header>






<div class="content--wrapper">
<div class="content--wrappermask"></div>




<div class="eventpage--wrapper">





<h3>
	The Christine Devitt Classroom
</h3>
     <p>
       The classroom is designed for small business meetings, seminars and or luncheons.  There is seating for up to 40 people, a lectern, speaker system, projector and screen. It is available Tuesday through Saturday for luncheon rental from 10am to 2pm or for all day seminar type meeting from 8am to 5pm  Contact the museum office 806-747-8734 for current rental rates.</p>

<img src="@asset('images/homepage__rentals--bg.png')" >

                               <h3> United Commons Patio</h3>

<div style="width:100%">
          <a href="http://138.197.114.217/rental-information/"> <p>Catering, Rental and Vendor Information</p></a>


  

</div>

                               <p>

                The covered patio is 6816 square feet of banquet space.  Rental includes the use of the museum’s tables and chairs and there are enough of them to seat 200 people.  There is a full service kitchen, however; the museum does not do any catering, you may bring in your own food or hire the caterer of your choice. There is a lectern on wheels that ties into a speaker system in the ceiling. One wall is treated as a screen for presentations and there is electricity in the floor for AV equipment.  Three large garage doors can be opened (weather permitting) for an indoor / outdoor facility.  Some of the events that have been held in the facility are:  
</p>
<p>
                <ul>

              <li>  Weddings</li>
              <li>  Birthday Parties</li>
           <li> Receptions</li>
        <li>Anniversary Celebrations</li>
    <li>Company Parties</li>
<li>Banquets</li>
</ul>
<ul>
<li>Civic Events</li>
<li>Proms</li>
<li>Trade Shows</li>
<li>Customer Appreciation Banquets</li>
<li>Awards Ceremonies</li>
<li>Reunions</li>
</ul>
</p>



<img  src="@asset('images/events__wedding.png')" >

<h5>See our <a href="http://138.197.114.217/calendar/">events calendar</a> for availability or contact the museum at <a href="tel:8067478734">806.747.8734</a>. Booking accepted up to one year in advance of the event</h5>
</div>

<!--===================================
=           Page content            =
======================================================================================================-->



</div>



@stop

