{{--
  Template Name: Visit
--}}

@extends('layouts.app')
@section('content')

<header class="page__hero">
<!--==========================
=            logo            =
===========================-->



<div class="hero__tagline"><span class="tagline__caption">Walk Among the</span><h2>Giants</h2><span class="tagline__subtitle">At The American Windmill Museum</span>
</div>





</header>




<!--================================
=            Admissions            =
=================================-->






<div class="content--wrapper">

<div class="content--wrappermask"></div>

	@php(the_content())
<!--===================================
=           Page content            =
======================================================================================================-->



</div>



@stop

