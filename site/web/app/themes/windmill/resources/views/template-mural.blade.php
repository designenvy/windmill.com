{{--
  Template Name: Mural
--}}

@extends('layouts.app')
@section('content')

<header class="page__hero">
<!--==========================
=            logo            =
===========================-->



<div class="hero__tagline"><span class="tagline__caption">Walk Among the</span><h2>Giants</h2><span class="tagline__subtitle">At The American Windmill Museum</span>
</div>





</header>




<!--================================
=            Admissions            =
=================================-->






<div class="content--wrapper">

<div class="content--wrappermask"></div>
<!--================================
=            Mural           =
=================================-->


<div class="mural--wrapper">

<section class="mural">




		<div class="mural__text">
		
<h2>  Legacy of the Wind Mural</h2>
<p>
This large art work covers two interior walls of the Museum. These walls were specially prepared to accommodate the slight movement of the building in high winds. 290 precision cut 4-foot by 5-foot aluminum panels were fastened to the original corrugated steel walls, then primed giving a flat surface for painting. The artist worked with the Museum’s Director, Coy Harris, to develop a comprehensive windmill story reflecting the relations of humans, the environment and technology in using the wind to help do work. The variety of windmills painted in the mural were selected from the museum’s large collection of historic windmills. </p>

	</div>








</section>

<div class="mural__photo"  ></div>

</div>






<!--===================================
=           Page content            =
======================================================================================================-->



</div>



@stop

