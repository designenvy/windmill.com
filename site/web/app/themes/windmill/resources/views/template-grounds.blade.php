{{--
  Template Name: Grounds
--}}

@extends('layouts.app')
@section('content')

<header class="page__hero">
<!--==========================
=            logo            =
===========================-->



<div class="hero__tagline"><span class="tagline__caption">Walk Among the</span><h2>Giants</h2><span class="tagline__subtitle">At The American Windmill Museum</span>
</div>





</header>




<!--================================
=            Admissions            =
=================================-->






<div class="content--wrapper">

<div class="content--wrappermask"></div>



	<div class="windmill--wrapper">

<section class="windmill" >
	


	<div class="windmill__text"  data-aos="fade-up"   data-aos-duration="800">
		
<h2>Windmills</h2>
<p> On the 28 acre park grounds sits over 70 historic windmills.  Some of these mills actually pump water from a well and others are set up to recirculate water from a tank, while others are simply showing their historic beauty.</p>

		

	</div>
<div class="windmill__photo"  data-aos="fade-up"   data-aos-duration="2000">
	</div>


</section>



</div>


<div class="windmill__divider">
	
</div>




<section class="flowerdew">

<div class="flowerdew__photo">
	</div>

	<div class="flowerdew__text">
		<h2>Flowerdew Hundred Post Mill</h2>

<p>Hundred Postmill is now assembled on the grounds of  the American Windmill Museum.</p>
<p>
The first windmill to be built in North America was constructed in 1621 for Sir George Yeardley on his plantation, Flowerdew Hundred, in Virginia.  The original windmill was destroyed in a storm, but remains were found in later excavations.</p>


		<p>

		In 1978 a new, commemorative windmill was built at Flowerdew Hundred incorporating features that illustrate the development of English windmill technology through the 18th century.  In the summer of 2010 this commemorative mill was moved from Virginia to its current home in Lubbock, Texas and is one of the very few working Post-Mills in the Western Hemisphere</p>

	</div>

</section>




<div class="millstone__photo"  ></div>

<!--================================
=            Mill Stone            =
=================================-->


<div class="millstone--wrapper">

<section class="millstone">




		<div class="millstone__text">
		
<h2>Garrison Family Millstone Collection</h2>
<p>
  Upon acquisition of the Flowerdew Hundred Post Mill, we also acquired the Millstones.  It was through a generous grant from the Garrison Family Foundation that we were able to relocate the stones from Virginia.  Fun Fact… The Hershey chocolate company used grist stones to grind the coco beans for the chocolate.	<img src="@asset('images/visit__millstone2.jpg')">
</p>
	</div>





</section>
</div>

<!--===================================
=           Page content            =
======================================================================================================-->



</div>



@stop

