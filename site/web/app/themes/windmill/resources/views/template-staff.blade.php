{{--
  Template Name: Staff
--}}

@extends('layouts.app')
@section('content')

<header class="page__hero">
<!--==========================
=            logo            =
===========================-->



<div class="hero__tagline"><h2><?php the_title(); ?></h2>
</div>





</header>

<div class="content--wrapper">
	<div class="content--wrappermask"></div>


<section class="staff">




  <?php 
     $loop = new WP_Query( array( 
        'post_type' => 'staff',   /* edit this line */
        'posts_per_page' => -1,
        'orderby' => 'title', 
        'order'   => 'ASC' ) );
?>

<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>  
	<div class="team__container">
<div class="team__overlay"></div>
	<div class="team__photo" style="background:url('<?php the_field('staff__photo')?>');">
			</div>
	
	<div class="team__title">
			<h3 ><?php the_title(); ?></h3>
			<p class="lead position"><?php the_field('titles'); ?></p>
			
</div>

</div>
<?php endwhile; ?>   



  


</section>

</div>

@stop

