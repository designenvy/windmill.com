@extends('layouts.app')
@section('content')
@include ('partials.header')
  @while(have_posts()) @php(the_post())
    @include('partials.content-page')
  @endwhile
@endsection
