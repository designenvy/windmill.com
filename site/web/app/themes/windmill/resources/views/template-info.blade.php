{{--
  Template Name: Info
--}}

@extends('layouts.app')
@section('content')

<header class="page__hero">
<!--==========================
=            logo            =
===========================-->



<div class="hero__tagline"><span class="tagline__caption">Walk Among the</span><h2>Giants</h2><span class="tagline__subtitle">At The American Windmill Museum</span>
</div>





</header>




<!--================================
=            Admissions            =
=================================-->






<div class="content--wrapper">

<div class="content--wrappermask"></div>
<div class="admission">
		<div class="info__admission">
		<h3>Admission</h3>
<ul>
<li><strong>Adults :   </strong> <span class="info__price">$7.50 </span></li>
<li><strong>Children (5 – 12 yr) : </strong> <span class="info__price">$5</span></li>
<li><strong>Family of four : </strong> <span class="info__price">$20</span></li>
<li><strong>Seniors / Veterans: </strong> <span class="info__price">$6</span></li>
</ul>
<small>Active Duty Military and their household families are admitted free with Military I.D.</small>
		</div>
	<svg  data-aos="fade-up" data-aos-duration="2500" class="windmill__left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116.48 114.29"><defs><style>.headera{fill:#3D365A;}</style></defs><title>hero__secondarywindmill--animated</title><path class="headera" d="M92.41,61.13l20.34,0.39L111.6,45.34,91.69,48.61a34.34,34.34,0,0,0-1.61-4.94l17.83-9.51L99,20.6,83.27,33.12a34.66,34.66,0,0,0-3.83-3.53l10.92-17L76,5.12,68.33,23.73a35,35,0,0,0-4.06-1L66.13,2.61l-16.22,0,1.8,20.1q-1.57.28-3.13,0.71L41.44,4.18l-14.63,7L37.34,28.81c-0.42.31-.84,0.64-1.25,1L22,15,11.32,27.21l16.57,12c-0.52.89-1,1.79-1.46,2.73L7.26,34.82,3,50.47,23.2,54q-0.09,1.09-.12,2.18L2.57,57.61l2.57,16L25,68.53a34.48,34.48,0,0,0,2,4.63L9.8,84.44l10,12.73L34.8,83a34.5,34.5,0,0,0,4,3.06l-9.63,18.32,15,6.23,6.15-19.71a35.1,35.1,0,0,0,5,.74l0.51,20.68,16.11-1.86L67.68,90.3a34.51,34.51,0,0,0,4.75-1.78l10.47,17.73,13.16-9.47L82.66,81.34A34.75,34.75,0,0,0,86,77.45l17.67,10.31,6.88-14.69L91.42,66.18A35.16,35.16,0,0,0,92.41,61.13Zm-2.94,4.35L76.56,60.82l13.78,0.26A33.08,33.08,0,0,1,89.47,65.48ZM69.79,67.17l-6.41,4.6-7.84.89-7.28-3-4.87-6.21-1.14-7.18,1.83-6.66,4.53-5.16L53.76,42l7.76,0,6.78,3.56,4.31,6.61L73.15,60ZM89.65,48.94L76,51.19l12.27-6.54A32.31,32.31,0,0,1,89.65,48.94Zm-8-14.53L70.76,43.06l7.55-11.73A32.85,32.85,0,0,1,81.65,34.41Zm-14.1-8.79L63,36.77l1.11-12A32.85,32.85,0,0,1,67.55,25.62ZM51.9,24.71l0.89,9.95L49.31,25.3Q50.59,24.95,51.9,24.71ZM38.4,30.59L41,34.92l-3.48-3.64Zm-8.84,9.85,7.57,5.49-8.76-3.26C28.74,41.91,29.14,41.17,29.57,40.44ZM25.24,54.38l6.84,1.2-6.93.48Q25.17,55.22,25.24,54.38ZM27.06,68l13.06-3.34L28.83,72A32.43,32.43,0,0,1,27.06,68ZM36.3,81.54L46,72.31,39.76,84.2A32.53,32.53,0,0,1,36.3,81.54Zm18.29,7.95a33.27,33.27,0,0,1-3.68-.59l4-12.8,0.33,13.44ZM67.27,88.3L64.5,75.09l6.89,11.66A32.33,32.33,0,0,1,67.27,88.3Zm14-8.52L72.39,69.52l11.81,6.89A32.71,32.71,0,0,1,81.3,79.78Z"/></svg>
</div>




<!--==============================
=            Windmill            =
===============================-->


<div class="info__photo">
	

</div>




<div class="store--wrapper">

<section class="store">

<h3> On the 28 acre park grounds sits over 70 historic windmills.  Some of these mills actually pump water from a well and others are set up to recirculate water from a tank, while others are simply showing their historic beauty.</h3>


</section>
</div>



<!--================================
=            store           =
=================================-->
<div class="store__photo"  ></div>

<div class="store--wrapper">

<section class="store">




		<div class="store__text">
		
<h2>Gift Shop</h2>
<p>
The Gift shop features a wide variety of wind related souvenirs as well as train related items.  From spinners to glassware and model windmills to model trains, you are sure to find something to take home to remind you of your visit to the museum.  Possibly a link to the online store. </p>

	</div>








</section>



</div>






<!--===================================
=           Page content            =
======================================================================================================-->



</div>



@stop

