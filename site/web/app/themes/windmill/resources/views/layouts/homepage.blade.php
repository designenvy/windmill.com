<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>






  <div class="container--main" >





          <main class="main anim_element anim_element--slideInUp" role="main" id="main"  >
           @yield('content')
           </main>
@include ('partials.nav')
   </div>


@php(do_action('get_footer'))
  @include('partials.footer')
  @php(wp_footer())



  </body>
</html>
