<div class="content--wrapper">
<div class="content--wrappermask"></div>

<article @php(post_class())>

  	<?php $thumb = get_the_post_thumbnail_url(); ?>
<img class="post__photo" src="<?php echo $thumb;?>">

  <div class="entry-content">
    @php(the_content())
  </div>

</article>

</div>