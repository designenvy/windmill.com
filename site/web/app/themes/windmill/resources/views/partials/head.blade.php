<head>
  <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6854356/7192792/css/fonts.css" />
<link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">

<link rel="shortcut icon" href="@asset('images/fav/favicon.ico')" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="57x57" href="@asset('images/fav/apple-touch-icon-57x57.png')">
<link rel="apple-touch-icon" sizes="60x60" href="@asset('images/fav/apple-touch-icon-60x60.png')">
<link rel="apple-touch-icon" sizes="72x72" href="@asset('images/fav/apple-touch-icon-72x72.png')">
<link rel="apple-touch-icon" sizes="76x76" href="@asset('images/fav/apple-touch-icon-76x76.png')">
<link rel="apple-touch-icon" sizes="114x114" href="@asset('images/fav/apple-touch-icon-114x114.png')">
<link rel="apple-touch-icon" sizes="120x120" href="@asset('images/fav/apple-touch-icon-120x120.png')">
<link rel="apple-touch-icon" sizes="144x144" href="@asset('images/fav/apple-touch-icon-144x144.png')">
<link rel="apple-touch-icon" sizes="152x152" href="@asset('images/fav/apple-touch-icon-152x152.png')">
<link rel="apple-touch-icon" sizes="180x180" href="@asset('images/fav/apple-touch-icon-180x180.png')">
<link rel="icon" type="image/png" href="@asset('images/fav/favicon-16x16.png')" sizes="16x16">
<link rel="icon" type="image/png" href="@asset('images/fav/favicon-32x32.png')" sizes="32x32">
<link rel="icon" type="image/png" href="@asset('images/fav/favicon-96x96.png')" sizes="96x96">
<link rel="icon" type="image/png" href="@asset('images/fav/android-chrome-192x192.png')" sizes="192x192">
<meta name="msapplication-square70x70logo" content="@asset('images/fav/smalltile.png')" />
<meta name="msapplication-square150x150logo" content="@asset('images/fav/mediumtile.png')" />
<meta name="msapplication-wide310x150logo" content="@asset('images/fav/widetile.png')" />
<meta name="msapplication-square310x310logo" content="@asset('images/fav/largetile.png')" />


  @php(wp_head())


</head>

