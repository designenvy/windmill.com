<?php
/**
 * Signature field.
 *
 * @package    WPFormsSignatures
 * @author     WPForms
 * @since      1.0.0
 * @license    GPL-2.0+
 * @copyright  Copyright (c) 2016, WPForms LLC
*/
class WPForms_Field_Signature extends WPForms_Field {

	/**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function init() {

		// Define field type information
		$this->name  = __( 'Signature', 'wpforms' );
		$this->type  = 'signature';
		$this->icon  = 'fa-pencil';
		$this->order = 30;
		$this->group = 'fancy';

		add_action( 'wpforms_frontend_js',            array( $this, 'frontend_js'           )        );
		add_action( 'wpforms_frontend_css',           array( $this, 'frontend_css'          )        );
		add_action( 'wpforms_builder_enqueues',       array( $this, 'builder_css'           )        );
		add_filter( 'wpforms_field_new_default',      array( $this, 'field_defaults'        )        );
		add_filter( 'wpforms_html_field_value',       array( $this, 'field_html_value'      ), 10, 4 );
	}

	/**
	 * Enqueue frontend field js.
	 *
	 * @since 1.0.0
	 * @param array $forms
	 */
	public function frontend_js( $forms ) {

		if ( wpforms()->frontend->assets_global() || true == wpforms_has_field_type( 'signature', $forms, true ) )  {

			wp_enqueue_script(
				'wpforms-signature_pad',
				plugin_dir_url( __FILE__ ) . 'assets/js/signature_pad.min.js',
				array(),
				'1.5.3',
				true
			);

			wp_enqueue_script(
				'wpforms-signature',
				plugin_dir_url( __FILE__ ) . 'assets/js/wpforms-signatures.js',
				array( 'jquery', 'wpforms-signature_pad' ),
				WPFORMS_SIGNATURES_VERSION,
				true
			);

		}
	}

	/**
	 * Enqueue frontend field CSS.
	 *
	 * @since 1.0.0
	 * @param array $forms
	 */
	public function frontend_css( $forms ) {

		if ( wpforms()->frontend->assets_global() || true == wpforms_has_field_type( 'signature', $forms, true ) )  {

			wp_enqueue_style(
				'wpforms-signature',
				plugin_dir_url( __FILE__ ) . 'assets/css/wpforms-signatures.css',
				array(),
				WPFORMS_SIGNATURES_VERSION
			);
		}
	}

	/**
	 * Enqueue form builder field js.
	 *
	 * @since 1.0.0
	 */
	public function builder_css() {

		wp_enqueue_style(
			'wpforms-builder-signatures',
			plugin_dir_url( __FILE__ ) . 'assets/css/admin-builder-signatures.css',
			array(),
			WPFORMS_SIGNATURES_VERSION
		);
	}

	/**
	 * Field defaults when creating new field.
	 *
	 * Default size to large.
	 *
	 * @since 1.0.0
	 * @param array $field
	 */
	function field_defaults( $field ) {

		if ( 'signature' == $field['type'] && empty( $field['size'] ) ) {
			$field['size'] = 'large';
		}

		return $field;
	}

	/**
	 * Return signature link/image for HTML supported values.
	 *
	 * @since 1.0.0
	 * @param string $value
	 * @param array $field
	 * @param array $form_data
	 * @param string $context
	 * @return string
	 */
	function field_html_value( $value, $field, $form_data = '', $context = '' ) {

		if ( !empty( $field['value'] ) && 'signature' == $field['type']  ) {

			$value = sanitize_text_field( $field['value'] );

			return sprintf( '<a href="%s" rel="noopener" target="_blank" style="max-width:500px;display:block;margin:0;"><img src="%s" style="max-width:100%%;display:block;margin:0;"></a>', $value, $value );
		}

		return $value;
	}

	/**
	 * Field options panel inside the builder.
	 *
	 * @since 1.0.0
	 * @param array $field
	 */
	public function field_options( $field ) {

		//--------------------------------------------------------------------//
		// Basic field options
		//--------------------------------------------------------------------//

		// Options open markup
		$this->field_option( 'basic-options', $field, array( 'markup' => 'open' ) );

		// Label
		$this->field_option( 'label', $field );

		// Description
		$this->field_option( 'description', $field );

		// Required toggle
		$this->field_option( 'required', $field );

		// Options close markup
		$this->field_option( 'basic-options', $field, array( 'markup' => 'close' ) );

		//--------------------------------------------------------------------//
		// Advanced field options
		//--------------------------------------------------------------------//

		// Options open markup
		$this->field_option( 'advanced-options', $field, array( 'markup' => 'open' ) );

		// Size
		$this->field_option( 'size', $field );

		// Hide label
		$this->field_option( 'label_hide', $field );

		// Custom CSS classes
		$this->field_option( 'css', $field );

		// Options close markup
		$this->field_option( 'advanced-options', $field, array( 'markup' => 'close' ) );
	}

	/**
	 * Field preview inside the builder.
	 *
	 * @since 1.0.0
	 * @param array $field
	 */
	public function field_preview( $field ) {

		$placeholder = !empty( $field['placeholder'] ) ? esc_attr( $field['placeholder'] ) : '';

		$this->field_preview_option( 'label', $field );

		echo '<div class="wpforms-signature-wrap"></div>';

		$this->field_preview_option( 'description', $field );
	}

	/**
	 * Field display on the form front-end.
	 *
	 * @since 1.0.0
	 * @param array $field
	 * @param array $form_data
	 */
	public function field_display( $field, $field_atts, $form_data ) {

		// Setup and sanitize the necessary data
		$field             = apply_filters( 'wpforms_signature_field_display', $field, $field_atts, $form_data );
		$field_required    = !empty( $field['required'] ) ? ' required' : '';
		$field_class       = implode( ' ', array_map( 'sanitize_html_class', $field_atts['input_class'] ) );
		$field_id          = implode( ' ', array_map( 'sanitize_html_class', $field_atts['input_id'] ) );
		?>

		<div class="wpforms-signature-wrap <?php echo $field_class; ?>">

			<canvas class="wpforms-signature-canvas"></canvas>

			<button class="wpforms-signature-clear" title="<?php _e( 'Clear Signature', 'wpforms-signature' ); ?>"><?php _e( 'Clear Signature', 'wpforms-signature' ); ?>"</button>

		</div>

		<input type="text" name="wpforms[fields][<?php echo $field['id']; ?>]" data-rule-signature="true" class="wpforms-signature-input" id="<?php echo $field_id; ?>" <?php echo $field_required; ?> hidden>
		<?php
	}

	/**
	 * Validates signature on form submit.
	 *
	 * @since 1.0.0
	 * @param int $field_id
	 * @param array $field_submit
	 * @param array $form_data
	 */
	public function validate( $field_id, $field_submit, $form_data ) {

		// Basic required check - If field is marked as required, check for entry data
		if ( !empty( $form_data['fields'][$field_id]['required'] ) && empty( $field_submit ) && '0' != $field_submit ) {

			wpforms()->process->errors[$form_data['id']][$field_id] = apply_filters( 'wpforms_required_label', __( 'This field is required', 'wpforms' ) );
			return;
		}

		// Simple format check
		if ( !empty( $field_submit ) && substr( $field_submit , 0, 22 ) !== 'data:image/png;base64,' ) {

			wpforms()->process->errors[$form_data['id']][$field_id] = __( 'Invalid signature image format', 'wpforms-signature' );
			return;
		}

		// Image check
		$base = str_replace( 'data:image/png;base64,', '', $field_submit );

		if ( !empty( $field_submit ) && ! imagecreatefromstring( base64_decode( $base ) ) ) {

			wpforms()->process->errors[$form_data['id']][$field_id] = __( 'Invalid signature image format', 'wpforms-signature' );
			return;
		}
	}

	/**
	 * Formats and sanitizes field.
	 *
	 * @since 1.0.0
	 * @param int $field_id
	 * @param array $field_submit
	 * @param array $form_data
	 */
	public function format( $field_id, $field_submit, $form_data ) {

		$name                 = !empty( $form_data['fields'][$field_id]['label'] ) ? sanitize_text_field( $form_data['fields'][$field_id]['label'] ) : '';
		$value                = sanitize_text_field( $field_submit );
		$uploads              = wp_upload_dir();
		$form_directory       = absint( $form_data['id'] ) . '-' . md5( $form_data['id'] . $form_data['created'] );
		$wpforms_uploads_root = trailingslashit( $uploads['basedir'] ) . 'wpforms';
		$wpforms_uploads_form = trailingslashit( $wpforms_uploads_root ) . $form_directory;
		$file_name            = sanitize_file_name( __( 'signature', 'wpforms_signature' ) . '-' . uniqid() . '.png' );
		$file_new             = trailingslashit( $wpforms_uploads_form ) . $file_name;
		$file_url             = trailingslashit( $uploads['baseurl'] ) . 'wpforms/' . trailingslashit( $form_directory ) . $file_name;

		// Double check we have a image passed
		if ( !empty( $value ) && substr( $value , 0, 22 ) == 'data:image/png;base64,' ) {

			// Check for form upload directory destination
			if ( ! file_exists( $wpforms_uploads_form ) ) {
				wp_mkdir_p( $wpforms_uploads_form );
			}

			// Check if the index.html exists in the root uploads director, if not create it
			if ( ! file_exists( trailingslashit( $wpforms_uploads_root ) . 'index.html' ) ) {
				file_put_contents( trailingslashit( $wpforms_uploads_root ) . 'index.html', '' );
			}

			// Check if the index.html exists in the form uploads director, if not create it
			if ( ! file_exists( trailingslashit( $wpforms_uploads_form ) . 'index.html' ) ) {
				file_put_contents( trailingslashit( $wpforms_uploads_form ) . 'index.html', '' );
			}

			// Compile image data
			$data = base64_decode( preg_replace( '#^data:image/\w+;base64,#i', '', $value ) );

			// Save image
			$save_signature = file_put_contents( $file_new, $data );

			if ( false === $save_signature ) {

				$value = '';

				wpforms_log(
					__( 'Upload Error, could not upload signature', 'wpforms_signature' ),
					$file_url,
					array( 'type' => array( 'entry', 'error' ), 'form_id' => $form_data['id'] )
				);

			} else {

				// Everything's done, so we provide the URL to the image
				$value = $file_url;

				// Set correct file permissions.
				$stat = stat( dirname( $file_new ));
				$perms = $stat['mode'] & 0000666;
				@ chmod( $file_new, $perms );
			}
		} else {

			$value = '';
		}

		wpforms()->process->fields[$field_id] = array(
			'name'  => $name,
			'value' => $value,
			'id'    => absint( $field_id ),
			'type'  => $this->type,
		);
	}
}
new WPForms_Field_Signature;