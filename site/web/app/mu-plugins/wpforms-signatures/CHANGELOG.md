Change Log
All notable changes to this project will be documented in this file, formatted via [this recommendation](http://keepachangelog.com/).

## [1.0.3] - 2016-08-01
### Fixed
- Signature reset when scrolling on some mobile devices

## [1.0.2] - 2016-02-24
### Fixed
- CSS not correctly applying if using only Base styling

## [1.0.1] - 2016-11-16
### Changed
- Signature is stored and created in an image file, instead of a data URI

### Fixed
- Signatures not working when hidden on load (conditional logic, pagination)
- Signatures not displaying in Gmail/Google Apps

## [1.0.0] - 2016-11-10
- Initial release
