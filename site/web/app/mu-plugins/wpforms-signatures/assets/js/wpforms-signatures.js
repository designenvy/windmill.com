;(function($) {

	var WPFormsSignatures = {

		signatures: {
			windowWidth: false
		},

		/**
		 * Start the engine.
		 *
		 * @since 1.0.0
		 */
		init: function() {

			$(document).on('wpformsReady', WPFormsSignatures.loadSignatures);

			$(document).on('click', '.wpforms-signature-clear', function(event) {
				event.preventDefault();
				WPFormsSignatures.clearSignature($(this));
			});
		},

		/**
		 * Load the signatures on the page.
		 *
		 * @since 1.0.0
		 */
		loadSignatures: function() {

			$('.wpforms-signature-canvas').each(function(index, el) {

				var $this  = $(this),
					$wrap  = $this.closest('.wpforms-field-signature'),
					$input = $wrap.find('.wpforms-signature-input'),
					id     = $wrap.attr('id'),
					canvas = $this.get(0);

				WPFormsSignatures.signatures[id] = new SignaturePad(canvas, {
					onEnd: function() {
						var imageData = WPFormsSignatures.cropSignatureCanvas(canvas);
						$input.val(imageData).trigger('input').trigger('change').valid();
					}
				});
			});

			$(window).resize(WPFormsSignatures.resizeSignatureCanvas);
			WPFormsSignatures.resizeSignatureCanvas();
		},

		/**
		 * Resizes canvas.
		 *
		 * @since 1.0.0
		 */
		resizeSignatureCanvas: function() {

			if ( ! WPFormsSignatures.signatures.windowWidth ) {
				WPFormsSignatures.signatures.windowWidth = $(window).width();
			} else {
				if ( WPFormsSignatures.signatures.windowWidth === $(window).width() ) {
					return;
				}
			}

			$('.wpforms-signature-canvas').each(function(index, el) {

				var $this        = $(this),
					$wrap        = $this.closest('.wpforms-field-signature'),
					$input       = $wrap.find('.wpforms-signature-input'),
					id           = $wrap.attr('id'),
					canvas       = $this.get(0),
					ratio        = Math.max(window.devicePixelRatio || 1, 1),
					canvasWidth  = 0,
					canvasHeight = 0;

				if ( $this.is(':hidden') ) {
					var sizes = WPFormsSignatures.getHiddenDimensions($this);
					canvasWidth = sizes.width;
					canvasHeight = sizes.height;
				} else {
					canvasWidth = canvas.offsetWidth;
					canvasHeight = canvas.offsetHeight;
				}

				canvas.width = canvasWidth * ratio;
				canvas.height = canvasHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);

				WPFormsSignatures.signatures[id].clear();
				$input.val('');
			});
		},

		/**
		 * Clear signature.
		 *
		 * @since 1.0.0
		 */
		clearSignature: function(el) {

			var $this  = $(el),
				$wrap  = $this.closest('.wpforms-field-signature'),
				$input = $wrap.find('.wpforms-signature-input'),
				id     = $wrap.attr('id');

			WPFormsSignatures.signatures[id].clear();
			$input.val('').trigger('input').trigger('change');
		},

		/**
		 * Crop signature canvas to only contain the signature and no whitespace.
		 *
		 * @since 1.0.0
		 */
		cropSignatureCanvas: function(canvas) {

			// First duplicate the canvas to not alter the original
			var croppedCanvas = document.createElement('canvas'),
				croppedCtx    = croppedCanvas.getContext('2d');

				croppedCanvas.width  = canvas.width;
				croppedCanvas.height = canvas.height;
				croppedCtx.drawImage(canvas, 0, 0);

			// Next do the actual cropping
			var w         = croppedCanvas.width,
				h         = croppedCanvas.height,
				pix       = {x:[], y:[]},
				imageData = croppedCtx.getImageData(0,0,croppedCanvas.width,croppedCanvas.height),
				x, y, index;

			for (y = 0; y < h; y++) {
				for (x = 0; x < w; x++) {
					index = (y * w + x) * 4;
					if (imageData.data[index+3] > 0) {

						pix.x.push(x);
						pix.y.push(y);

					}
				}
			}
			pix.x.sort(function(a,b){return a-b});
			pix.y.sort(function(a,b){return a-b});
			var n = pix.x.length-1;

			w = pix.x[n] - pix.x[0];
			h = pix.y[n] - pix.y[0];
			var cut = croppedCtx.getImageData(pix.x[0], pix.y[0], w, h);

			croppedCanvas.width = w;
			croppedCanvas.height = h;
			croppedCtx.putImageData(cut, 0, 0);

			return croppedCanvas.toDataURL();
		},

		/**
		 * If canvas or parent is hidden (display:none) attempt to get the
		 * width/height.
		 *
		 * @since 1.1.0
		 */
		getHiddenDimensions: function(el) {

			var $this    = $(el);
				$wrapper = $this.closest('.wpforms-field-container');

			if ($this.length == 0) {
				return false;
			}
			var $clone = $this.clone()
				.show()
				.css('visibility','hidden')
				.appendTo($wrapper);

			var result = {
				width:      $clone.innerWidth(),
				height:     $clone.innerHeight(),
				offsetTop:  $clone.offset().top,
				offsetLeft: $clone.offset().left
			};
			$clone.remove();
			return result;
		}
	}

	WPFormsSignatures.init();

	window.wpforms_signatures = WPFormsSignatures;
})(jQuery);
